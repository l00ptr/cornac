from textwrap import dedent

import pytest


def test_parse_mail_dsn():
    from cornac.core.config import parse_mail_dsn

    assert parse_mail_dsn('console:') == {'MAIL_BACKEND': 'console'}

    assert parse_mail_dsn('file:///a/b') == {
        'MAIL_BACKEND': 'file',
        'MAIL_FILE_PATH': '/a/b',
    }

    assert parse_mail_dsn('smtps://admin@example.com:qwerty@server:123') == {
        'MAIL_BACKEND': 'smtp',
        'MAIL_SERVER': 'server',
        'MAIL_PORT': 123,
        'MAIL_USERNAME': 'admin@example.com',
        'MAIL_PASSWORD': 'qwerty',
        'MAIL_USE_SSL': True,
        'MAIL_USE_TLS': False,
    }

    assert parse_mail_dsn('smtp://localhost?tls=1') == {
        'MAIL_BACKEND': 'smtp',
        'MAIL_SERVER': 'localhost',
        'MAIL_PORT': None,
        'MAIL_USERNAME': None,
        'MAIL_PASSWORD': None,
        'MAIL_USE_SSL': False,
        'MAIL_USE_TLS': True,
    }

    with pytest.raises(ValueError):
        parse_mail_dsn('https://locahost:8080')


def test_append_creds():
    from cornac.core.config.writer import append_credentials

    config = ""
    with pytest.raises(ValueError):
        append_credentials(config, 'K', 'S')

    config = "CREDENTIALS = 'string'"
    with pytest.raises(ValueError):
        append_credentials(config, 'K', 'S')

    config = "CREDENTIALS = {}"
    new = append_credentials(config, 'K', 'S', comment=None)
    wanted = dedent("""\
    CREDENTIALS = {
        'K': 'S',
    }
    """)
    assert wanted == str(new)

    config = dedent("""\
    CREDENTIALS = {
        # before comment
        'OLDKEY': 'OLDSECRET'  # line comment
        # end comment
    }
    """)
    new = append_credentials(config, 'KEY', 'SECRET', comment='new comment')
    wanted = dedent("""\
    CREDENTIALS = {
        # before comment
        'OLDKEY': 'OLDSECRET',  # line comment
        # end comment
        # new comment
        'KEY': 'SECRET',
    }
    """)
    assert wanted == str(new)
