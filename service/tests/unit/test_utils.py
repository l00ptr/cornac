import pytest


def test_pwgen(mocker):
    from cornac.utils import pwgen, validate_password_strength

    assert 32 == len(pwgen())
    choice = mocker.patch('cornac.utils.choice')
    choice.side_effect = list('toooweak' * 4) + list('Str0nG__' * 4)
    validate_password_strength(pwgen())


def test_password_strength():
    from cornac.utils import validate_password_strength

    with pytest.raises(ValueError) as ei:
        validate_password_strength("Short!")
    assert " 8 chars" in str(ei.value)

    with pytest.raises(ValueError) as ei:
        validate_password_strength("\n\n\n\n\n\n\n\t")
    assert "special char" in str(ei.value).lower()

    bad = [
        'onlylower',
        'ONLYUPPER',
        '!!!!!!!!',
        'UPPERlower',
        'UPPER!!!!',
        'lower!!!!',
    ]

    for password in bad:
        with pytest.raises(ValueError) as ei:
            validate_password_strength(password)


def test_patch_dict():
    from cornac.utils import patch_dict

    my = dict(modified=1, unmodified=2)

    with patch_dict(my, modified=3, new=4):
        assert 2 == my['unmodified']
        assert 3 == my['modified']
        assert 4 == my['new']

    assert 1 == my['modified']
    assert 'new' not in my
