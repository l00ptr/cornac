import csv
import os.path
import re
from sh import cornac

import pytest


def test_help():
    out = cornac("--help")
    assert 'migratedb' in out
    assert 'worker' in out


def test_bootstrap(cornac_env, rds):
    cornac(
        "--verbose", "bootstrap",
        "--root", "remi.root@acme.tld",
        _err_to_out=True, _env=cornac_env)


@pytest.mark.slow
def test_recover(iaas, cornac_env):
    iaas.stop_machine('cornac', tenant=cornac_env['_ROOT_TENANT'], zone='a')
    cornac("--verbose", "recover", _err_to_out=True, _env=cornac_env)


def test_generate_credentials(cornac_env):
    path = os.path.abspath("tests-func-tmp-config.py")
    if os.path.exists(path):
        os.unlink(path)

    env = dict(
        cornac_env,
        # Seed credentials for preconfiguration.
        CORNAC_ACCESS_KEY_ID='CIDATESTROOTACCESS',
        CORNAC_SECRET_ACCESS_KEY='notsecret',
    )
    out = cornac("--verbose", "generate-credentials", _env=env)

    reader = csv.reader(out.splitlines())
    lines = list(reader)
    assert 2 == len(lines)
    _, _, access_key, secret_key, *_ = lines[1]

    assert 'CIDATESTROOTACCESS' == access_key
    assert 'notsecret' == secret_key


def test_reset_root_password(cornac_env, maildir, cornacc, rds):
    r = cornacc.post(
        '/cornac/reset_password/', json=dict(username='remi.root@acme.tld')
    )
    r.raise_for_status()
    msgfile, = list(maildir.iterdir())
    with open(msgfile) as f:
        msg = f.read()
    m = re.search(r"http://.+:8001(/.+)\n", msg)
    assert m is not None, "Failed to find reset password URL"
    path = m.group(1)
    path = path.replace('cornac/#/', 'cornac/')

    r = cornacc.post(
        path,
        json=dict(password='notasecret')
    )
    r.raise_for_status()

    # The cookies returned by this login are preserved in HTTP session.
    r = cornacc.post(
        '/cornac/login',
        json=dict(username='remi.root@acme.tld', password='notasecret'),
    )
    r.raise_for_status()


def test_accounts(cornacc, rds):
    r = cornacc.post('/cornac/accounts', json=dict(Alias='Prod'))
    r.raise_for_status()

    r = cornacc.post('/cornac/accounts', json=dict(Alias='Tmp'))
    r.raise_for_status()
    tmp = r.json()['account']
    tmp_uri = f"/cornac/accounts/{tmp['AccountId']}"

    r = cornacc.get('/cornac/accounts')
    r.raise_for_status()
    assert 3 == len(r.json()['accounts'])

    r = cornacc.patch(tmp_uri, json=dict(NewAlias='Tmp2'))
    r.raise_for_status()

    r = cornacc.delete(tmp_uri)
    r.raise_for_status

    r = cornacc.get('/cornac/accounts')
    r.raise_for_status()
    assert 2 == len(r.json()['accounts'])


def test_users(aws, cornac_env, rds):
    aws('iam', 'create-user', '--user-name', 'arnaud.admin@acme.tld')
    aws('iam', 'create-user', '--user-name', 'denis.dba@acme.tld')
    aws('iam', 'create-user', '--user-name', 'odile.other@acme.tld')

    # List users.
    out = aws('iam', 'list-users')
    users = out['Users']
    assert 4 == len(users)

    env = dict(
        cornac_env,
        # Seed credentials for preconfiguration.
        CORNAC_ACCESS_KEY_ID='CIDATESTOTHERACCESS',
        CORNAC_SECRET_ACCESS_KEY='notsecret',
    )
    cornac(
        "--verbose", "generate-credentials",
        "--user-name", "odile.other@acme.tld",
        _env=env,
    )


def test_virtual_ips(cornacc, rds):
    r = cornacc.get('/cornac/virtual-ip-ranges')
    r.raise_for_status()
    assert 0 == len(r.json()['ranges'])

    r = cornacc.post(
        '/cornac/virtual-ip-ranges',
        json=dict(range='10.0.0.0/30', comment='For change.'),
    )
    r.raise_for_status()
    j = r.json()
    assert '10.0.0.0/30' == j['range']['range']
    assert 'For change.' == j['range']['comment']

    r = cornacc.get('/cornac/virtual-ip-ranges')
    r.raise_for_status()
    assert 1 == len(r.json()['ranges'])

    r = cornacc.patch(
        f"/cornac/virtual-ip-ranges/{j['range']['id']}",
        json=dict(comment='For deletion.'),
    )
    r.raise_for_status()
    j = r.json()
    assert 'For deletion.' == j['range']['comment']

    r = cornacc.delete(
        f"/cornac/virtual-ip-ranges/{j['range']['id']}",
    )
    r.raise_for_status()

    r = cornacc.get('/cornac/virtual-ip-ranges')
    r.raise_for_status()
    assert 0 == len(r.json()['ranges'])

    test_range = os.environ.get('CORNAC_TEST_VIRTUAL_IP_RANGE')
    if test_range:
        r = cornacc.post(
            '/cornac/virtual-ip-ranges',
            json=dict(range=test_range, comment='Test range.'),
        )
        r.raise_for_status()
