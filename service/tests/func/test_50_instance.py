import logging
from contextlib import contextmanager
from time import sleep

import psycopg2
import pytest
from sh import cornac


logger = logging.getLogger(__name__)
PGPASSWORD = 'C0nf\'de tie!'  # The baddest password to send thru SSH.


@contextmanager
def pgconnect(instance, **kw):
    kw = dict(
        host=instance['Endpoint']['Address'],
        port=instance['Endpoint']['Port'],
        user=instance['MasterUsername'],
        dbname=instance['MasterUsername'],
        **kw
    )
    with psycopg2.connect(**kw) as conn:
        with conn.cursor() as curs:
            yield curs


def test_describe_db_instances(aws, rds):
    out = aws("rds", "describe-db-instances")
    assert 'postgres' == out['DBInstances'][0]['Engine']


def test_describe_orderable_db_instance_options(aws, rds):
    out = aws(
        "rds", "describe-orderable-db-instance-options",
        "--engine", "postgres",
    )
    assert 4 < len(out['OrderableDBInstanceOptions'])
    assert 'postgres' == out['OrderableDBInstanceOptions'][0]['Engine']


def test_create_db_instance(aws, cornac_env, rds, worker):
    if 'CORNAC_TEMBOARD' in cornac_env:
        args = ["--enable-performance-insights"]
    else:
        args = []

    out = aws(
        "rds", "create-db-instance",
        "--db-instance-identifier", "test0",
        "--db-instance-class", "db.t2.micro",
        "--availability-zone", "localb",
        "--engine", "postgres",
        "--engine-version", "11",
        "--allocated-storage", "5",
        "--no-multi-az",
        "--master-username", "master",
        "--master-user-password", PGPASSWORD,
        *args
    )
    assert 'creating' == out['DBInstance']['DBInstanceStatus']

    aws.wait_instance('available')


def test_sql_to_endpoint(aws, rds, iaas):
    out = aws(
        "rds", "describe-db-instances", "--db-instance-identifier", "test0")
    with pgconnect(out['DBInstances'][0], password=PGPASSWORD) as curs:
        curs.execute("SELECT NOW()")


def test_temboard(temboardc, cornacc):
    r = cornacc.get(
        '/cornac/temboard/redirect/000000000001/test0',
        allow_redirects=False
    )
    assert r.is_redirect
    r = temboardc.get(r.headers['location'])
    r.raise_for_status()


@pytest.mark.slow
def test_reboot_db_instance(aws, rds, worker):
    out = aws(
        "rds", "reboot-db-instance",
        "--db-instance-identifier", "test0",
    )
    assert 'rebooting' == out['DBInstance']['DBInstanceStatus']

    aws.wait_instance('available')

    with pgconnect(out['DBInstance'], password=PGPASSWORD) as curs:
        curs.execute("SELECT NOW()")


@pytest.mark.slow
def test_inspect_stopped(aws, cornac_env, iaas, rds):
    iaas.stop_machine('test0', tenant=cornac_env['_ROOT_TENANT'], zone='b')
    cornac("--verbose", "inspect", _err_to_out=True, _env=cornac_env)
    out = aws(
        "rds", "describe-db-instances",
        "--db-instance-identifier", "test0",
    )
    instance, = out['DBInstances']
    assert 'test0' == instance['DBInstanceIdentifier']
    assert 'stopped' == instance['DBInstanceStatus']


def test_delete_db_instance(aws, iaas, rds, worker):
    out = aws(
        "rds", "delete-db-instance",
        "--db-instance-identifier", "test0",
        "--skip-final-snapshot",
    )
    assert 'deleting' == out['DBInstance']['DBInstanceStatus']

    for s in range(0, 60):
        sleep(s / 2.)
        try:
            out = aws(
                "rds", "describe-db-instances",
                "--db-instance-identifier", "test0")
            if 'deleting' == out['DBInstances'][0]['DBInstanceStatus']:
                continue
        except Exception as e:
            logger.warning("Can't describe db instance anymore: %s", e)
            break
    else:
        raise Exception("Timeout deleting database instance.")

    assert 1 == len(list(iaas.list_machines()))
