def test_assume_role(aws, cornacc, rds):
    cornacc.post(
        '/cornac/login',
        json=dict(username='remi.root@acme.tld', password='notasecret'))
    prod = cornacc.get('/cornac/accounts/000000000002')
    arn = prod.json()['account']['AdminRoleArn']
    out = aws(
        'sts', 'assume-role',
        '--role-arn', arn,
        '--role-session-name', 'unused',
    )

    assumed_aws = aws.with_credentials(
        out['Credentials']['AccessKeyId'],
        out['Credentials']['SecretAccessKey'],
    )

    out = assumed_aws('sts', 'get-caller-identity')
    assert arn == out['Arn']

    # There is not instances in this account.
    out = assumed_aws('rds', 'describe-db-instances')
    assert 0 == len(out['DBInstances'])
