import os

import pytest


if 'CORNAC_BACKUPS_LOCATION' not in os.environ:
    pytest.skip('No backup location.', allow_module_level=True)


def test_wait_auto_snapshot(aws, rds, worker):
    # Ensure that automated snapshot is done before creating a manual snapshot.
    # This avoid snapshot operation conflicts when chaingin bootstrap and
    # snaphot, skipping other tests.
    out = aws("rds", "describe-db-snapshots")
    identifier = out['DBSnapshots'][0]['DBSnapshotIdentifier']

    aws.wait_snapshot(identifier=identifier)


def test_create_snapshot(aws, rds, worker):
    out = aws(
        "rds", "create-db-snapshot",
        "--db-instance-identifier", "cornac",
        "--db-snapshot-identifier", "cornac-manual",
    )
    s = out['DBSnapshot']

    assert 'cornac-manual' == s['DBSnapshotIdentifier']
    assert 'creating' == s['Status']
    assert 'cornac' == s['DBInstanceIdentifier']
    assert 'locala' == s['AvailabilityZone']


def test_describe_and_wait_snapshots(aws, rds, worker):
    out = aws("rds", "describe-db-snapshots")
    assert 2 == len(out['DBSnapshots'])

    # Tests filter on DBSnapshotIdentifier.
    aws.wait_snapshot(identifier='cornac-manual')


def test_restore_from_snapshot(aws, rds, worker):
    out = aws(
        "rds", "restore-db-instance-from-db-snapshot",
        "--db-snapshot-identifier", "cornac-manual",
        "--db-instance-identifier", "cornac-rest-manual",
    )
    i = out['DBInstance']
    assert 'cornac-rest-manual' == i['DBInstanceIdentifier']
    assert 'creating' == i['DBInstanceStatus']
    assert 'locala' == i['AvailabilityZone']


def test_restore_to_point_in_time(aws, rds, worker):
    out = aws(
        "rds", "restore-db-instance-to-point-in-time",
        "--source-db-instance-identifier", "cornac",
        "--target-db-instance-identifier", "cornac-rest-pitr",
        "--use-latest-restorable-time",
        "--availability-zone", "localb",
    )
    i = out['DBInstance']
    assert 'cornac-rest-pitr' == i['DBInstanceIdentifier']
    assert 'creating' == i['DBInstanceStatus']
    assert 'localb' == i['AvailabilityZone']


def test_wait_and_check_restored_instances(aws, rds, worker):
    aws.wait_instance(identifier='cornac-rest-manual')
    aws.wait_instance(identifier='cornac-rest-pitr')

    out = aws("rds", "describe-db-snapshots")
    # 1 manual + 3 automated
    assert 4 == len(out['DBSnapshots'])


def test_delete_snapshot(aws, rds, worker):
    out = aws(
        "rds", "delete-db-snapshot",
        "--db-snapshot-identifier", "cornac-manual",
    )
    s = out['DBSnapshot']

    assert 'cornac-manual' == s['DBSnapshotIdentifier']
    assert 'deleted' == s['Status']
    assert 'cornac' == s['DBInstanceIdentifier']

    aws.wait_snapshot(None, identifier='cornac-manual')
