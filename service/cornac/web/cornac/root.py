import logging

from flask import Blueprint, request, redirect


logger = logging.getLogger(__name__)
root = Blueprint('root', __name__)


# For development purpose, we want /js, /css, etc. to behave like webpack
# devserver, with publicPath empty.
#
# However, we don't redirect / to /cornac until we get ansible modules able to
# access RDS endpoint somewhere else than /.


@root.route('/css/<path:filename>')
@root.route('/fonts/<path:filename>')
@root.route('/img/<path:filename>')
@root.route('/js/<path:filename>')
def assets(filename):
    return redirect('/cornac/static' + request.path)
