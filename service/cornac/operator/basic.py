# Apply actions on infrastructure.
#
# The concept of Operator is borrowed from K8S.
#

import logging
import pdb
import sys
from pathlib import Path

from . import Operator
from .. import worker
from ..iaas import IaaS
from ..ssh import Password, RemoteShell
from ..utils import pwgen


logger = logging.getLogger(__name__)


class BasicOperator(Operator):
    # Implementation using pghelper.sh

    helper = '/usr/local/bin/pghelper.sh'

    def create_db_instance(self, instance):
        machine_kw = instance.machine_dict
        self.iaas.create_machine(
            data_size_gb=instance.data['AllocatedStorage'],
            **machine_kw,
        )
        self.iaas.start_machine(**machine_kw)
        address = self.iaas.endpoint(**machine_kw)
        shell = RemoteShell('root', address)
        shell.wait()
        logger.debug("Sending helper script.")
        local_helper = str(Path(__file__).parent / 'pghelper.sh')
        shell.copy(local_helper, self.helper)

        # Formatting disk
        try:
            # Check whether Postgres VG is configured.
            shell(["test", "-d", "/dev/Postgres"])
        except Exception:
            dev = self.iaas.guess_data_device_in_guest(**machine_kw)
            logger.info("Preparing disk %s.", dev)
            shell([self.helper, "prepare-disk", dev])
            logger.info("Creating Postgres instance.")
            shell([
                self.helper, "create-instance",
                instance.data['EngineVersion'],
                instance.data['DBInstanceIdentifier'],
            ])
            shell([self.helper, "start"])
        else:
            logger.info("Reusing Postgres instance.")

        # Master user
        master = instance.data['MasterUsername']
        shell([
            self.helper,
            "create-masteruser", master,
            Password(instance.data['MasterUserPassword']),
        ])

        # Creating database
        dbname = master
        bases = shell([self.helper, "psql", "-l"])
        if f"\n {dbname} " in bases:
            logger.info("Reusing database %s.", dbname)
        else:
            logger.info("Creating database %s.", dbname)
            shell([self.helper, "create-database", dbname, master])

        instance.data['Endpoint'] = dict(Address=address, Port=5432)

    def create_db_snapshot(self, instance, snapshot):
        logger.warn("Snapshot not implemented.")

    def delete_db_snapshot(self, snapshot):
        logger.warn("Snapshot not implemented.")

    def restore_db_instance_from_db_snapshot(self, instance, snapshot):
        logger.warn("Snapshot not implemented.")
        instance.data['MasterUserPassword'] = pw = pwgen(8)
        logger.warn("Creating new instance with master password: '%s'.", pw)
        self.create_db_instance(instance)
        worker.recovery_end.send(instance.id)

    def restore_db_instance_to_point_in_time(self, target, source,
                                             restore_time):
        self.restore_db_instance_from_db_snapshot(target, None)

    def is_running(self, instance):
        # Check whether *Postgres* is running.
        try:
            address = self.iaas.endpoint(**instance.machine_dict)
            shell = RemoteShell('root', address)
            shell([self.helper, "psql", "-l"])
            return True
        except Exception as e:
            logger.debug("Failed to execute SQL in Postgres: %s.", e)
            return False


def test_main():
    # Hard coded real test case, for PoC development.

    from flask import current_app as app

    # What aws would send to REST API.
    command = {
        'DBInstanceIdentifier': 'cli0',
        'AllocatedStorage': 5,
        'DBInstanceClass': 'db.t2.micro',
        'Engine': 'postgres',
        'EngineVersion': '11',
        'MasterUsername': 'postgres',
        'MasterUserPassword': 'C0nfidentiel',
    }

    with IaaS.connect(app.config['IAAS'], app.config) as iaas:
        operator = BasicOperator(iaas, app.config)
        response = operator.create_db_instance(command)

    logger.info(
        "    psql -h %s -p %s -U %s -d %s",
        response['Endpoint']['Address'],
        response['Endpoint']['Port'],
        command['MasterUsername'],
        command['DBInstanceIdentifier'],
    )


if "__main__" == __name__:
    from cornac import create_app

    logging.basicConfig(
        format="%(levelname)5.5s %(message)s",
        level=logging.DEBUG,
    )
    app = create_app()
    try:
        with app.app_context():
            test_main()
    except pdb.bdb.BdbQuit:
        pass
    except Exception:
        logger.exception("Uhandled error:")
        pdb.post_mortem(sys.exc_info()[2])
